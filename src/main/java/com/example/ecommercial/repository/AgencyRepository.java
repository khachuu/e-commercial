package com.example.ecommercial.repository;

import com.example.ecommercial.model.Agency;
import com.example.ecommercial.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgencyRepository extends JpaRepository<Agency, Long> {
    Agency findOneByUserID(Long id);

    void deleteByUserID(Long id);
}
