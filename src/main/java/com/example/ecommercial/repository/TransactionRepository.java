package com.example.ecommercial.repository;

import com.example.ecommercial.model.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    Page<Transaction> findAllByCustomerID(Long customerId, Pageable pageable);

    Page<Transaction> findAllByAgencyID(Long agencyId, Pageable pageable);

    Transaction findOneByCustomerIDAndBillingID(Long customerId, Long billingID);

    Transaction findOneByAgencyIDAndBillingID(Long agencyID, Long billingID);
}
