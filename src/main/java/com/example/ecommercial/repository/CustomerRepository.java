package com.example.ecommercial.repository;

import com.example.ecommercial.model.Agency;
import com.example.ecommercial.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Customer findOneByUserID(Long id);
}
