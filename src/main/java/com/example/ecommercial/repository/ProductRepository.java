package com.example.ecommercial.repository;

import com.example.ecommercial.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    Page<Product> findAllByActivatedIsTrue(Pageable pageable);

    Optional<Product> findByIdAndActivatedIsTrue(Long id);
}
