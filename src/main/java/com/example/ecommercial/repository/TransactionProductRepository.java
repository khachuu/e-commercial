package com.example.ecommercial.repository;

import com.example.ecommercial.model.Transaction;
import com.example.ecommercial.model.TransactionProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionProductRepository extends JpaRepository<TransactionProduct, Long> {
    List<TransactionProduct> findAllByTransactionID(Long id);
}
