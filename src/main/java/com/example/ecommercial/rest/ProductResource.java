package com.example.ecommercial.rest;

import com.example.ecommercial.common.PaginationUtil;
import com.example.ecommercial.model.Product;
import com.example.ecommercial.repository.ProductRepository;
import com.example.ecommercial.security.RoleConstants;
import com.example.ecommercial.service.ProductService;
import com.example.ecommercial.service.dto.AdminAgencyDTO;
import com.example.ecommercial.service.dto.BillingDTO;
import com.example.ecommercial.service.dto.ProductDTO;
import com.example.ecommercial.service.exception.InvalidInputException;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/agency")
@Log4j2
public class ProductResource {
    private final ProductService productService;

    private final ProductRepository productRepository;

    public ProductResource(ProductService productService, ProductRepository productRepository) {
        this.productService = productService;
        this.productRepository = productRepository;
    }

    @PostMapping("/products")
    @PreAuthorize("hasAuthority(\"" + RoleConstants.AGENCY + "\")")
    public ResponseEntity<Product> createProduct(@Valid @RequestBody ProductDTO productDTO) {
        log.debug("REST request to save Product : {}", productDTO);

        if (productDTO.getId() != null) {
            throw new InvalidInputException();
        } else {
            Product newProduct = productService.create(productDTO);
            return ResponseEntity.ok(newProduct);
        }
    }

    @PutMapping("/products")
    @PreAuthorize("hasAuthority(\"" + RoleConstants.AGENCY + "\")")
    public ResponseEntity<AdminAgencyDTO> updateProduct(@Valid @RequestBody ProductDTO productDTO) {
        log.debug("REST request to update Product : {}", productDTO);

        Optional<Product> existingProduct = productRepository.findById(productDTO.getId());
        if (existingProduct.isPresent() && (!existingProduct.orElseThrow().getId().equals(productDTO.getId()))) {
            throw new InvalidInputException();
        }
        productService.update(productDTO);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("/products")
    @PreAuthorize("hasAuthority(\"" + RoleConstants.AGENCY + "\")")
    public ResponseEntity<List<ProductDTO>> getAllProducts(Pageable pageable) {
        final Page<ProductDTO> page = productService.filter(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/products/{id}")
    @PreAuthorize("hasAuthority(\"" + RoleConstants.AGENCY + "\")")
    public ResponseEntity<ProductDTO> getProduct(@PathVariable Long id) {
        ProductDTO productDTO = productService.findByIdAndActivatedIsTrue(id);
        return ResponseEntity.ok(productDTO);
    }

    @DeleteMapping("/products/{id}")
    @PreAuthorize("hasAuthority(\"" + RoleConstants.AGENCY + "\")")
    public ResponseEntity<Void> deleteProduct(@PathVariable Long id) {
        productService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
