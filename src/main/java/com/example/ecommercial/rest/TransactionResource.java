package com.example.ecommercial.rest;

import com.example.ecommercial.common.PaginationUtil;
import com.example.ecommercial.model.Transaction;
import com.example.ecommercial.security.RoleConstants;
import com.example.ecommercial.service.TransactionService;
import com.example.ecommercial.service.dto.TransactionDTO;
import com.example.ecommercial.service.exception.InvalidInputException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/api/transaction")
public class TransactionResource {
    private final TransactionService transactionService;

    public TransactionResource(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping("")
    @PreAuthorize("hasAuthority(\"" + RoleConstants.CUSTOMER + "\")")
    public ResponseEntity<Transaction> createTransaction(@RequestBody TransactionDTO transactionDTO) {
        if (transactionDTO.getId() != null) {
            throw new InvalidInputException();
        } else {
            Transaction transaction = transactionService.create(transactionDTO);
            return ResponseEntity.ok(transaction);
        }
    }

    @GetMapping("")
    @PreAuthorize("hasAnyAuthority(\"" + RoleConstants.AGENCY + "\",\"" + RoleConstants.CUSTOMER + "\" )")
    public ResponseEntity<List<TransactionDTO>> getListTransaction(Pageable pageable) {
        final Page<TransactionDTO> page = transactionService.getTransactionList(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
}
