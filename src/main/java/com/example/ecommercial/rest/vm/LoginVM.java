package com.example.ecommercial.rest.vm;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class LoginVM {

    private String username;

    private String password;

    private boolean rememberMe;

    @Override
    public String toString() {
        return "LoginVM{" +
            "username='" + username + '\'' +
            ", rememberMe=" + rememberMe +
            '}';
    }
}
