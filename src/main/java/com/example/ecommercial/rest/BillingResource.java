package com.example.ecommercial.rest;

import com.example.ecommercial.security.RoleConstants;
import com.example.ecommercial.service.BillingService;
import com.example.ecommercial.service.dto.BillingDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/billing")
public class BillingResource {
    private final BillingService billingService;

    public BillingResource(BillingService billingService) {
        this.billingService = billingService;
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyAuthority(\"" + RoleConstants.AGENCY + "\",\"" + RoleConstants.ADMIN + "\", \"" + RoleConstants.CUSTOMER + "\" )")
    public ResponseEntity<BillingDTO> getByID(@PathVariable Long id) {
        BillingDTO billingDTO = billingService.getById(id);
        return ResponseEntity.ok(billingDTO);
    }

}
