package com.example.ecommercial.rest.errors;

import org.springframework.core.NestedRuntimeException;

public class LoginAlreadyUsedException extends NestedRuntimeException {

    public LoginAlreadyUsedException() {
        super("Login name already used!");
    }
}
