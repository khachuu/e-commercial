package com.example.ecommercial.rest;

import com.example.ecommercial.model.Cart;
import com.example.ecommercial.security.RoleConstants;
import com.example.ecommercial.service.CartService;
import com.example.ecommercial.service.dto.CartDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/cart")
public class CartResource {
    private final CartService cartService;

    public CartResource(CartService cartService) {
        this.cartService = cartService;
    }

    @PutMapping("")
    @PreAuthorize("hasAuthority(\"" + RoleConstants.CUSTOMER + "\")")
    public ResponseEntity<Cart> updateCart(@RequestBody CartDTO cartDTO) {
        Cart cart = cartService.createOrUpdate(cartDTO);
        return ResponseEntity.ok(cart);
    }

    @GetMapping("")
    @PreAuthorize("hasAuthority(\"" + RoleConstants.CUSTOMER + "\")")
    public ResponseEntity<CartDTO> getCurrentCart() {
        CartDTO cartDTO = cartService.getCurrentCart();
        return ResponseEntity.ok(cartDTO);
    }
}
