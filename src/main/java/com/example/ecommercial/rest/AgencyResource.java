package com.example.ecommercial.rest;

import com.example.ecommercial.common.PaginationUtil;
import com.example.ecommercial.model.Agency;
import com.example.ecommercial.model.User;
import com.example.ecommercial.repository.UserRepository;
import com.example.ecommercial.rest.errors.LoginAlreadyUsedException;
import com.example.ecommercial.security.RoleConstants;
import com.example.ecommercial.service.AgencyService;
import com.example.ecommercial.service.dto.AdminAgencyDTO;
import com.example.ecommercial.service.dto.AgencyDTO;
import com.example.ecommercial.service.exception.InvalidInputException;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/admin")
@Log4j2
public class AgencyResource {
    private final AgencyService agencyService;

    private final UserRepository userRepository;

    public AgencyResource(AgencyService agencyService, UserRepository userRepository) {
        this.agencyService = agencyService;
        this.userRepository = userRepository;
    }

    @PostMapping("/agencies")
    @PreAuthorize("hasAuthority(\"" + RoleConstants.ADMIN + "\")")
    public ResponseEntity<Agency> createAgency(@Valid @RequestBody AdminAgencyDTO adminAgencyDTO) {
        log.debug("REST request to save Agency : {}", adminAgencyDTO);

        if (adminAgencyDTO.getId() != null) {
            throw new InvalidInputException();
        } else if (userRepository.findOneByUsername(adminAgencyDTO.getUsername().toLowerCase()).isPresent()) {
            throw new LoginAlreadyUsedException();
        } else {
            Agency newUser = agencyService.createAgency(adminAgencyDTO);
            return ResponseEntity.ok(newUser);
        }
    }

    @PutMapping("/agencies")
    @PreAuthorize("hasAuthority(\"" + RoleConstants.ADMIN + "\")")
    public ResponseEntity<AdminAgencyDTO> updateAgency(@Valid @RequestBody AdminAgencyDTO adminAgencyDTO) {
        agencyService.updateAgency(adminAgencyDTO);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("/agencies")
    @PreAuthorize("hasAuthority(\"" + RoleConstants.ADMIN + "\")")
    public ResponseEntity<List<AgencyDTO>> getAll(Pageable pageable) {
        final Page<AgencyDTO> page = agencyService.filter(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/agencies/{id}")
    @PreAuthorize("hasAuthority(\"" + RoleConstants.ADMIN + "\")")
    public ResponseEntity<AgencyDTO> getAgency(@PathVariable Long id) {
        log.debug("REST request to get Agency : {}", id);
        return agencyService.getById(id).map(AgencyDTO::new).map((response) -> ResponseEntity.status(HttpStatus.OK).body(response)).orElseThrow(()
                -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/agencies/{id}")
    @PreAuthorize("hasAuthority(\"" + RoleConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteUser(@PathVariable Long id) {
        agencyService.deleteAgency(id);
        return ResponseEntity.noContent().build();
    }
}
