package com.example.ecommercial.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories({ "com.example.ecommercial.repository" })
@EnableTransactionManagement
public class DatabaseConfiguration {}
