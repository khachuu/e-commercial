package com.example.ecommercial.security;

/**
 * Constants for Spring Security authorities.
 */
public final class RoleConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String CUSTOMER = "ROLE_CUSTOMER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    public static final String AGENCY = "ROLE_AGENCY";

}
