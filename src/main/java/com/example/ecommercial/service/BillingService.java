package com.example.ecommercial.service;

import com.example.ecommercial.model.Agency;
import com.example.ecommercial.model.Billing;
import com.example.ecommercial.model.Customer;
import com.example.ecommercial.model.Transaction;
import com.example.ecommercial.repository.BillingRepository;
import com.example.ecommercial.repository.TransactionRepository;
import com.example.ecommercial.service.constant.BillingStatus;
import com.example.ecommercial.service.constant.TransactionStatus;
import com.example.ecommercial.service.dto.BillingDTO;
import com.example.ecommercial.service.dto.TransactionDTO;
import com.example.ecommercial.service.exception.InvalidInputException;
import com.example.ecommercial.service.exception.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;

@Service
@Transactional
public class BillingService {
    private final BillingRepository billingRepository;

    private final TransactionRepository transactionRepository;

    private final UserService userService;

    public BillingService(BillingRepository billingRepository, TransactionRepository transactionRepository, UserService userService) {
        this.billingRepository = billingRepository;
        this.transactionRepository = transactionRepository;
        this.userService = userService;
    }

    public Billing create(Long transactionID, BillingDTO billingDTO) {
        Optional<Transaction> transactionOptional = transactionRepository.findById(transactionID);
        if (transactionOptional.isEmpty()) {
            throw new ResourceNotFoundException();
        }
        Transaction transaction = transactionOptional.get();
        if (!TransactionStatus.CREATED.equals(transaction.getStatus())) {
            throw new InvalidInputException();
        }
        Billing billing = new Billing();
        billing.setStatus(BillingStatus.CREATED);
        billing.setCreatedAt(Instant.now());
        billing.setPaymentMethod(billingDTO.getPaymentMethod());
        billing.setAmount(billingDTO.getAmount());

        return billingRepository.save(billing);
    }

    public BillingDTO getById(Long id) {

        Optional<Billing> billingOptional = billingRepository.findById(id);
        Customer customer = userService.getCurrentCustomer();
        if (customer != null) {
           Transaction transaction = transactionRepository.findOneByCustomerIDAndBillingID(customer.getId(), id);
           if (transaction == null) {
               throw new ResourceNotFoundException();
           }
        }

        Agency agency = userService.getCurrentAgency();
        if (agency != null) {
            Transaction transaction = transactionRepository.findOneByAgencyIDAndBillingID(agency.getId(), id);
            if (transaction == null) {
                throw new ResourceNotFoundException();
            }
        }
        if (billingOptional.isPresent()) {
            return new BillingDTO(billingOptional.get());
        }
        throw new ResourceNotFoundException();
    }

}
