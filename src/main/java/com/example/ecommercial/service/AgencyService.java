package com.example.ecommercial.service;

import com.example.ecommercial.common.RandomUtil;
import com.example.ecommercial.model.Agency;
import com.example.ecommercial.model.Role;
import com.example.ecommercial.model.User;
import com.example.ecommercial.repository.AgencyRepository;
import com.example.ecommercial.repository.RoleRepository;
import com.example.ecommercial.repository.UserRepository;
import com.example.ecommercial.security.RoleConstants;
import com.example.ecommercial.security.SecurityUtils;
import com.example.ecommercial.service.dto.AdminAgencyDTO;
import com.example.ecommercial.service.dto.AdminUserDTO;
import com.example.ecommercial.service.dto.AgencyDTO;
import com.example.ecommercial.service.exception.ResourceNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
@Log4j2
public class AgencyService {

    private final PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;

    private final AgencyRepository agencyRepository;

    private final RoleRepository roleRepository;

    private final UserService userService;

    public AgencyService(PasswordEncoder passwordEncoder, UserRepository userRepository, AgencyRepository agencyRepository, RoleRepository roleRepository, UserService userService) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.agencyRepository = agencyRepository;
        this.roleRepository = roleRepository;
        this.userService = userService;
    }

    public void updateAgency(AdminAgencyDTO adminAgencyDTO) {
        Optional<Agency> agencyOptional = agencyRepository.findById(adminAgencyDTO.getId());
        if (agencyOptional.isPresent()) {
            Agency agency = agencyOptional.get();
            agency.setPhoneNumber(adminAgencyDTO.getPhoneNumber());
            agency.setTaxNumber(adminAgencyDTO.getTaxNumber());
            agency.setAddress(adminAgencyDTO.getAddress());
            agency.setName(adminAgencyDTO.getName());

            agencyRepository.save(agency);

            Optional<User> userOptional = userRepository.findById(agency.getUserID());
            if (userOptional.isPresent()) {
                User user = userOptional.get();
                user.setUpdatedAt(Instant.now());
                user.setEmail(adminAgencyDTO.getEmail());
                userRepository.save(user);
            } else {
                throw new ResourceNotFoundException();
            }
        } else {
            throw new ResourceNotFoundException();
        }
    }

    public Agency createAgency(AdminAgencyDTO adminAgencyDTO) {
        User user = new User();
        if (adminAgencyDTO.getEmail() != null) {
            user.setEmail(adminAgencyDTO.getEmail().toLowerCase());
        }
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        user.setUsername(adminAgencyDTO.getUsername());
        user.setPassword(encryptedPassword);
        user.setActivated(true);
        Set<Role> roles = new HashSet<>();
        Role agencyRole = roleRepository.findById(RoleConstants.AGENCY).isPresent() ?
                roleRepository.findById(RoleConstants.AGENCY).get() : null;
        roles.add(agencyRole);
        user.setAuthorities(roles);
        user = userRepository.save(user);
        log.debug("Created Information for User: {}", user);

        Agency agency = new Agency();
        agency.setUserID(user.getId());
        agency.setName(adminAgencyDTO.getName());
        agency.setTaxNumber(adminAgencyDTO.getTaxNumber());
        agency.setPhoneNumber(adminAgencyDTO.getPhoneNumber());
        agency.setAddress(adminAgencyDTO.getAddress());

        return agencyRepository.save(agency);
    }

    @Transactional(readOnly = true)
    public Optional<Agency> getById(Long id) {
        return agencyRepository.findById(id);
    }

    public Page<AgencyDTO> filter(Pageable pageable) {
        return agencyRepository.findAll(pageable).map(AgencyDTO::new);
    }

    @Transactional
    public void deleteAgency(Long id) {
        Optional<Agency> agency = agencyRepository.findById(id);
        if (agency.isPresent()) {
            userRepository.deleteById(agency.get().getUserID());
            agencyRepository.delete(agency.get());
        } else {
            throw new ResourceNotFoundException();
        }
    }


}
