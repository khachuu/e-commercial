package com.example.ecommercial.service;

import com.example.ecommercial.model.*;
import com.example.ecommercial.repository.ProductRepository;
import com.example.ecommercial.repository.TransactionProductRepository;
import com.example.ecommercial.repository.TransactionRepository;
import com.example.ecommercial.service.constant.TransactionStatus;
import com.example.ecommercial.service.dto.BillingDTO;
import com.example.ecommercial.service.dto.TransactionDTO;
import com.example.ecommercial.service.dto.TransactionProductDTO;
import com.example.ecommercial.service.exception.AccessDeniedException;
import com.example.ecommercial.service.exception.InvalidInputException;
import com.example.ecommercial.service.exception.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class TransactionService {
    private final TransactionRepository transactionRepository;

    private final ProductRepository productRepository;

    private final TransactionProductRepository transactionProductRepository;

    private final UserService userService;

    private final BillingService billingService;

    public TransactionService(TransactionRepository transactionRepository, ProductRepository productRepository, TransactionProductRepository transactionProductRepository, UserService userService, BillingService billingService) {
        this.transactionRepository = transactionRepository;
        this.productRepository = productRepository;
        this.transactionProductRepository = transactionProductRepository;
        this.userService = userService;
        this.billingService = billingService;
    }

    public Transaction create(TransactionDTO transactionDTO) {
        Customer customer = userService.getCurrentCustomer();
        if (customer == null) {
            throw new AccessDeniedException();
        }
        Transaction transaction = new Transaction();
        transaction.setCreatedAt(Instant.now());
        transaction.setStatus(TransactionStatus.CREATED);
        transaction.setAgencyID(transactionDTO.getAgencyID());
        transaction.setCustomerID(customer.getId());

        transaction = transactionRepository.save(transaction);

        BigDecimal totalPrice = BigDecimal.ZERO;

        List<TransactionProduct> transactionProductList = new ArrayList<>();
        for (TransactionProductDTO transactionProductDTO: transactionDTO.getProducts()) {
            Optional<Product> productOptional = productRepository.findById(transactionProductDTO.getProductID());
            if (productOptional.isEmpty()) {
                throw new ResourceNotFoundException();
            }
            if (!productOptional.get().getAgencyID().equals(transactionDTO.getAgencyID())) {
                throw new InvalidInputException();
            }
            TransactionProduct transactionProduct = new TransactionProduct();
            transactionProduct.setProductID(transactionProductDTO.getProductID());
            transactionProduct.setPrice(productOptional.get().getPrice());
            transactionProduct.setProductQuantity(transactionProductDTO.getProductQuantity());
            transactionProduct.setTransactionID(transaction.getId());
            totalPrice = totalPrice.add(productOptional.get().getPrice());
            transactionProductList.add(transactionProduct);
        }

        transactionProductRepository.saveAll(transactionProductList);

        BillingDTO billingDTO = new BillingDTO();
        billingDTO.setAmount(totalPrice);
        billingDTO.setPaymentMethod("COD");

        Billing billing = billingService.create(transaction.getId(), billingDTO);
        transaction.setBillingID(billing.getId());

        return transactionRepository.save(transaction);
    }

    public Page<TransactionDTO> getTransactionList(Pageable pageable) {
        Page<TransactionDTO> pages = null;
        Customer customer = userService.getCurrentCustomer();
        if (customer != null) {
            pages = transactionRepository.findAllByCustomerID(customer.getId(), pageable).map(TransactionDTO::new);
        }

        Agency agency = userService.getCurrentAgency();
        if (agency != null) {
            pages = transactionRepository.findAllByAgencyID(agency.getId(), pageable).map(TransactionDTO::new);
        }

        if (pages != null) {
            pages.stream().forEach(elm -> {
                List<TransactionProductDTO> setProducts = transactionProductRepository.findAllByTransactionID(elm.getId()).stream().map(TransactionProductDTO::new).toList();
                elm.setProducts(setProducts);
            });
            return pages;
        }

        throw new AccessDeniedException();
    }
}
