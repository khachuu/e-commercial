package com.example.ecommercial.service;

import com.example.ecommercial.model.Cart;
import com.example.ecommercial.model.CartProduct;
import com.example.ecommercial.model.Customer;
import com.example.ecommercial.repository.CartProductRepository;
import com.example.ecommercial.repository.CartRepository;
import com.example.ecommercial.service.dto.CartDTO;
import com.example.ecommercial.service.dto.CartProductDTO;
import com.example.ecommercial.service.exception.AccessDeniedException;
import com.example.ecommercial.service.exception.InvalidInputException;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CartService {
    private final CartRepository cartRepository;

    private final CartProductRepository cartProductRepository;

    private final UserService userService;

    public CartService(CartRepository cartRepository, CartProductRepository cartProductRepository, UserService userService) {
        this.cartRepository = cartRepository;
        this.cartProductRepository = cartProductRepository;
        this.userService = userService;
    }

    public Cart createOrUpdate(CartDTO cartDTO) {
        Customer customer = userService.getCurrentCustomer();
        if (customer == null) {
            throw new AccessDeniedException();
        }
        Cart cart = cartRepository.findByCustomerID(customer.getId());
        if (cart != null) {
            if (cartDTO == null) {
                throw new InvalidInputException();
            }
            cart.setUpdatedAt(Instant.now());
            cart = cartRepository.save(cart);
        } else {
            Cart newCart = new Cart();
            newCart.setCustomerID(customer.getId());
            newCart.setCreatedAt(Instant.now());
            cart = cartRepository.save(newCart);
        }

        // update list product to cart
        List<CartProduct> cartProductList = new ArrayList<>();
        for (CartProductDTO cartProductDTO : cartDTO.getProductDTOs()) {
            CartProduct cartProduct = new CartProduct();
            cartProduct.setCartID(cart.getId());
            cartProduct.setProductID(cartProductDTO.getProductID());
            cartProduct.setProductQuantity(cartProductDTO.getProductQuantity());
            cartProductList.add(cartProduct);
        }
        cartProductRepository.saveAll(cartProductList);

        return cart;
    }

    public CartDTO getCurrentCart() {
        Customer customer = userService.getCurrentCustomer();
        if (customer == null) {
            throw new AccessDeniedException();
        }

        Cart cart = cartRepository.findByCustomerID(customer.getId());

        if (cart == null) {
            cart = new Cart();
            cart.setCustomerID(customer.getId());
            cart.setCreatedAt(Instant.now());
            cart = cartRepository.save(cart);
            return new CartDTO(cart, null);
        } else {
            List<CartProductDTO> cartProductDTOS
                    = cartProductRepository.findAllByCartID(cart.getId()).stream().map(CartProductDTO::new).collect(Collectors.toList());
            return new CartDTO(cart, cartProductDTOS);
        }
    }
}
