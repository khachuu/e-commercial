package com.example.ecommercial.service;

import com.example.ecommercial.common.RandomUtil;
import com.example.ecommercial.model.Agency;
import com.example.ecommercial.model.Customer;
import com.example.ecommercial.model.Role;
import com.example.ecommercial.model.User;
import com.example.ecommercial.repository.AgencyRepository;
import com.example.ecommercial.repository.CustomerRepository;
import com.example.ecommercial.repository.RoleRepository;
import com.example.ecommercial.repository.UserRepository;
import com.example.ecommercial.security.RoleConstants;
import com.example.ecommercial.security.SecurityUtils;
import com.example.ecommercial.service.dto.AccountDTO;
import com.example.ecommercial.service.dto.AdminUserDTO;
import com.example.ecommercial.service.dto.UserDTO;
import com.example.ecommercial.service.exception.AccessDeniedException;
import com.example.ecommercial.service.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final RoleRepository roleRepository;

    private final AgencyRepository agencyRepository;

    private final CustomerRepository customerRepository;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository, AgencyRepository agencyRepository, CustomerRepository customerRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.agencyRepository = agencyRepository;
        this.customerRepository = customerRepository;
    }

    public Optional<AdminUserDTO> updateUser(AdminUserDTO userDTO) {
        return Optional
                .of(userRepository.findById(userDTO.getId()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(user -> {
                    if (userDTO.getEmail() != null) {
                        user.setEmail(userDTO.getEmail().toLowerCase());
                    }
                    user.setActivated(userDTO.isActivated());
                    Set<Role> managedAuthorities = user.getAuthorities();
                    managedAuthorities.clear();
                    userDTO
                            .getAuthorities()
                            .stream()
                            .map(roleRepository::findById)
                            .filter(Optional::isPresent)
                            .map(Optional::get)
                            .forEach(managedAuthorities::add);
                    userRepository.save(user);
                    log.debug("Changed Information for User: {}", user);
                    return user;
                })
                .map(AdminUserDTO::new);
    }

    public User registerUser(AdminUserDTO userDTO, String password) {
        userRepository
                .findOneByUsername(userDTO.getUsername().toLowerCase())
                .ifPresent(existingUser -> {
                    boolean removed = removeNonActivatedUser(existingUser);
                    if (!removed) {
                        throw new UsernameAlreadyUsedException();
                    }
                });

        User newUser = new User();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setUsername(userDTO.getUsername().toLowerCase());
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        if (userDTO.getEmail() != null) {
            newUser.setEmail(userDTO.getEmail().toLowerCase());
        }
        newUser.setActivated(false);
        Set<Role> authorities = new HashSet<>();
        roleRepository.findById(RoleConstants.CUSTOMER).ifPresent(authorities::add);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    //
    private boolean removeNonActivatedUser(User existingUser) {
        if (existingUser.isActivated()) {
            return false;
        }
        userRepository.delete(existingUser);
        userRepository.flush();
        return true;
    }

    public User createUser(AdminUserDTO userDTO) {
        User user = new User();

        if (userDTO.getEmail() != null) {
            user.setEmail(userDTO.getEmail().toLowerCase());
        }
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        user.setPassword(encryptedPassword);
        user.setActivated(true);
        if (userDTO.getAuthorities() != null) {
            Set<Role> authorities = userDTO
                    .getAuthorities()
                    .stream()
                    .map(roleRepository::findById)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .collect(Collectors.toSet());
            user.setAuthorities(authorities);
        }
        userRepository.save(user);
        log.debug("Created Information for User: {}", user);
        return user;
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneByUsername(login);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities() {
        return SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByUsername);
    }

    public Page<AdminUserDTO> filter(Pageable pageable) {
        return userRepository.findAll(pageable).map(AdminUserDTO::new);
    }

    public Optional<User> activateRegistration(String key) {
        // TODO
        return Optional.empty();
    }

    public void updateUser(AccountDTO accountDTO) {
        SecurityUtils
                .getCurrentUserLogin()
                .flatMap(userRepository::findOneByUsername)
                .ifPresent(user -> {
                    if (accountDTO.getEmail() != null) {
                        user.setEmail(accountDTO.getEmail().toLowerCase());
                    }
                    user.setUpdatedAt(Instant.now());
                    userRepository.save(user);
                    log.debug("Changed Information for User: {}", user);
                });
        Agency agency = getCurrentAgency();
        if (agency != null) {
            agency.setName(accountDTO.getName());
            agency.setAddress(accountDTO.getAddress());
            agency.setPhoneNumber(accountDTO.getPhoneNumber());
            agency.setTaxNumber(accountDTO.getTaxNumber());
            agencyRepository.save(agency);
        } else {
            Customer customer = getCurrentCustomer();
            if (customer != null) {
                customer.setName(accountDTO.getName());
                customer.setAddress(accountDTO.getAddress());
                customer.setPhoneNumber(accountDTO.getPhoneNumber());
                customer.setGender(accountDTO.getGender());
                customerRepository.save(customer);
            }
        }
    }

    public void deleteUser(String username) {
        userRepository.deleteByUsername(username);
    }

    public Agency getCurrentAgency() {
        if (SecurityUtils.getCurrentUserLogin().isPresent()) {
            Optional<User> user = userRepository.findOneByUsername(SecurityUtils.getCurrentUserLogin().get());
            if (user.isPresent()) {
                return agencyRepository.findOneByUserID(user.get().getId());
            }
        }
        throw new AccessDeniedException();
    }

    public Customer getCurrentCustomer() {
        if (SecurityUtils.getCurrentUserLogin().isPresent()) {
            Optional<User> user = userRepository.findOneByUsername(SecurityUtils.getCurrentUserLogin().get());
            if (user.isPresent()) {
                return customerRepository.findOneByUserID(user.get().getId());
            }
        }
        throw new AccessDeniedException();
    }

    public AccountDTO getCurrentUser() {
        Optional<User> userOptional = this.getUserWithAuthorities();
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            AccountDTO accountDTO = new AccountDTO();
            accountDTO.setId(user.getId());
            accountDTO.setAuthorities(user.getAuthorities().stream().map(Role::getName).collect(Collectors.toSet()));
            accountDTO.setEmail(user.getEmail());
            accountDTO.setUsername(accountDTO.getUsername());
            accountDTO.setCreatedAt(user.getCreatedDate());

            Agency agency = getCurrentAgency();
            if (agency != null) {
                accountDTO.setTaxNumber(agency.getTaxNumber());
                accountDTO.setPhoneNumber(agency.getPhoneNumber());
                accountDTO.setName(agency.getName());
            } else {
                Customer customer = getCurrentCustomer();
                if (customer != null) {
                    accountDTO.setPhoneNumber(customer.getPhoneNumber());
                    accountDTO.setGender(customer.getGender());
                    accountDTO.setName(customer.getName());
                }
            }
            return accountDTO;
        }
        throw new ResourceNotFoundException();
    }
}
