package com.example.ecommercial.service.dto;

import com.example.ecommercial.model.Cart;
import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

@Getter
@Setter
public class CartDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    @Column(name = "customer_id")
    private Long customerID;

    private Instant updatedAt;

    private Instant createdAt;

    List<CartProductDTO> productDTOs;

    public CartDTO(Cart cart, List<CartProductDTO> productDTOs) {
        this.productDTOs = productDTOs;
        this.id = cart.getId();
        this.customerID = cart.getCustomerID();
        this.createdAt = cart.getCreatedAt();
        this.updatedAt = cart.getUpdatedAt();
    }
}
