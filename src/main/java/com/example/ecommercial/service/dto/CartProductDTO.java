package com.example.ecommercial.service.dto;

import com.example.ecommercial.model.CartProduct;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CartProductDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long productID;

    private Long productQuantity;

    public CartProductDTO(CartProduct cartProduct) {
        this.productQuantity = cartProduct.getProductQuantity();
        this.productID = cartProduct.getProductID();
    }

}
