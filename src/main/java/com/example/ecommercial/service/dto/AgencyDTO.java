package com.example.ecommercial.service.dto;

import com.example.ecommercial.model.Agency;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AgencyDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long userID;

    private String name;

    private String address;

    private String phoneNumber;

    private String taxNumber;

    public AgencyDTO(Agency agency) {
        super();
        this.id = agency.getId();
        this.name = agency.getName();
        this.address = agency.getAddress();
        this.phoneNumber = agency.getPhoneNumber();
        this.taxNumber = agency.getTaxNumber();
        this.userID = agency.getUserID();
    }
}
