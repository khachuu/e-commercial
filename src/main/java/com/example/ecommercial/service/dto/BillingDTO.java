package com.example.ecommercial.service.dto;

import com.example.ecommercial.model.Billing;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

@Getter
@Setter
@NoArgsConstructor
public class BillingDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private BigDecimal amount;

    private String status;

    private Instant paymentAt;

    private String paymentMethod;

    private Instant updatedAt;

    private Instant createdAt;

    public BillingDTO(Billing billing) {
        this.id = billing.getId();
        this.amount = billing.getAmount();
        this.createdAt = billing.getCreatedAt();
        this.updatedAt = billing.getUpdatedAt();
        this.paymentAt = billing.getPaymentAt();
        this.status = billing.getStatus();
        this.paymentMethod = billing.getPaymentMethod();
    }
}
