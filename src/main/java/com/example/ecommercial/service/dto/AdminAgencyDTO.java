package com.example.ecommercial.service.dto;

import com.example.ecommercial.config.Constants;
import com.example.ecommercial.model.Agency;
import com.example.ecommercial.model.Role;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class AdminAgencyDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    @NotBlank
    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 255)
    private String username;

    @Size(max = 255)
    private String name;

    @Email
    @Size(min = 5, max = 254)
    private String email;

    private String address;

    private String phoneNumber;

    private String taxNumber;

    public AdminAgencyDTO(Agency agency) {
        this.id = agency.getId();
        this.name = agency.getName();
        this.address = agency.getAddress();
        this.phoneNumber = agency.getPhoneNumber();
        this.taxNumber = agency.getTaxNumber();
    }

}
