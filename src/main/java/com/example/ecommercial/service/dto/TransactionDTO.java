package com.example.ecommercial.service.dto;

import com.example.ecommercial.model.Transaction;
import jakarta.persistence.Column;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TransactionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long customerID;

    private String status;

    private Long billingID;

    private Long agencyID;

    private Instant updatedAt;

    private Instant createdAt;

    private List<TransactionProductDTO> products;

    public TransactionDTO(Transaction transaction) {
        this.id = transaction.getId();
        this.customerID = transaction.getCustomerID();
        this.agencyID = transaction.getAgencyID();
        this.createdAt = transaction.getCreatedAt();
        this.updatedAt = transaction.getUpdatedAt();
        this.billingID = transaction.getBillingID();
        this.status = transaction.getStatus();
    }
}
