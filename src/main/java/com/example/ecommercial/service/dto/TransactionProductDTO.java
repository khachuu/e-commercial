package com.example.ecommercial.service.dto;

import com.example.ecommercial.model.TransactionProduct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class TransactionProductDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long productID;

    private BigDecimal price;

    private Long productQuantity;

    private BigDecimal discount;

    private BigDecimal tax;

    private BigDecimal shippingFee;

    private BigDecimal otherFee;

    public TransactionProductDTO(TransactionProduct transactionProduct) {
        this.productID = transactionProduct.getProductID();
        this.price = transactionProduct.getPrice();
        this.productQuantity = transactionProduct.getProductQuantity();
        this.tax = transactionProduct.getTax();
        this.shippingFee = transactionProduct.getShippingFee();
        this.discount = transactionProduct.getDiscount();
        this.otherFee = transactionProduct.getOtherFee();
    }

}
