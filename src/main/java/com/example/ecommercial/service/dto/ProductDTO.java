package com.example.ecommercial.service.dto;

import com.example.ecommercial.model.Product;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

@Getter
@Setter
@NoArgsConstructor
public class ProductDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long agencyID;

    private String name;

    private String description;

    private BigDecimal price;

    private Long stockQuantity;

    private Instant createdAt;

    private Instant updatedAt;

    public ProductDTO(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.price = product.getPrice();
        this.agencyID = product.getAgencyID();
        this.description = product.getDescription();
        this.stockQuantity = product.getStockQuantity();
        this.createdAt = product.getCreatedAt();
        this.updatedAt = product.getUpdatedAt();
    }

}
