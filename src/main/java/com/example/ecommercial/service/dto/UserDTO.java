package com.example.ecommercial.service.dto;

import com.example.ecommercial.model.Role;
import com.example.ecommercial.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.Instant;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A DTO representing a user, with only the public attributes.
 */

@Getter
@Setter
@NoArgsConstructor
public class UserDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String username;

    private String email;

    private boolean activated;

    private Instant createdDate;

    private Instant updatedDate;

    private Set<String> authorities;

    public UserDTO(User user) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.email = user.getEmail();
        this.activated = user.isActivated();
        this.createdDate = user.getCreatedDate();
        this.updatedDate = user.getUpdatedAt();
        this.authorities = user.getAuthorities().stream().map(Role::getName).collect(Collectors.toSet());
    }
}
