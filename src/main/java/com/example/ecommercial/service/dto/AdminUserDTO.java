package com.example.ecommercial.service.dto;

import com.example.ecommercial.config.Constants;
import com.example.ecommercial.model.Role;
import com.example.ecommercial.model.User;
import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.Instant;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A DTO representing a user, with his authorities.
 */
@Getter
@Setter
public class AdminUserDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    @NotBlank
    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 50)
    private String username;

    @Size(max = 255)
    private String name;

    @Email
    @Size(min = 5, max = 254)
    private String email;

    private boolean activated = false;

    private Instant createdAt;

    private Instant updatedAt;

    private Set<String> authorities;

    public AdminUserDTO() {
    }

    public AdminUserDTO(User user) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.email = user.getEmail();
        this.activated = user.isActivated();
        this.updatedAt = user.getUpdatedAt();
        this.createdAt = user.getCreatedDate();
        this.authorities = user.getRoles().stream().map(Role::getName).collect(Collectors.toSet());
    }

    public void setAuthorities(Set<String> authorities) {
        this.authorities = authorities;
    }

}
