package com.example.ecommercial.service.constant;

public class TransactionStatus {
    public static final String CREATED = "created";
    public static final String BILL_CREATED = "bill_created";

    public static final String FAILED = "failed";

}
