package com.example.ecommercial.service.exception;

public class AccessDeniedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public AccessDeniedException() {
        super("access denied");
    }
}
