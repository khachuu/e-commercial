package com.example.ecommercial.service.exception;

public class InvalidInputException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public InvalidInputException() {
        super("invalid input");
    }
}
