package com.example.ecommercial.service;

import com.example.ecommercial.model.Agency;
import com.example.ecommercial.model.Product;
import com.example.ecommercial.repository.ProductRepository;
import com.example.ecommercial.service.dto.ProductDTO;
import com.example.ecommercial.service.exception.AccessDeniedException;
import com.example.ecommercial.service.exception.ResourceNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class ProductService {

    private final ProductRepository productRepository;

    private final UserService userService;

    public ProductService(ProductRepository productRepository, UserService userService) {
        this.productRepository = productRepository;
        this.userService = userService;
    }


    public void update(ProductDTO productDTO) {
        Optional<Product> product = productRepository.findById(productDTO.getId());
        Agency agency = userService.getCurrentAgency();
        if (agency == null) {
            throw new AccessDeniedException();
        }
        if (product.isPresent() && agency.getUserID().equals(productDTO.getId())) {
            product.get().setUpdatedAt(Instant.now());
            product.get().setName(productDTO.getName());
            product.get().setPrice(productDTO.getPrice());
            product.get().setDescription(productDTO.getDescription());
            product.get().setStockQuantity(productDTO.getStockQuantity());

            productRepository.save(product.get());
        }
    }

    public Product create(ProductDTO productDTO) {
        Agency agency = userService.getCurrentAgency();
        if (agency == null) {
            throw new AccessDeniedException();
        }
        Product product = new Product();
        product.setAgencyID(agency.getId());
        product.setUpdatedAt(Instant.now());
        product.setName(productDTO.getName());
        product.setPrice(productDTO.getPrice());
        product.setDescription(productDTO.getDescription());
        product.setStockQuantity(productDTO.getStockQuantity());
        product.setCreatedAt(Instant.now());
        product.setActivated(true);

        return productRepository.save(product);
    }

    @Transactional(readOnly = true)
    public ProductDTO findByIdAndActivatedIsTrue(Long id) {
        Optional<Product> productOptional = productRepository.findByIdAndActivatedIsTrue(id);
        if (productOptional.isPresent()) {
            return new ProductDTO(productOptional.get());
        }
        throw new ResourceNotFoundException();
    }

    public Page<ProductDTO> filter(Pageable pageable) {
        return productRepository.findAllByActivatedIsTrue(pageable).map(ProductDTO::new);
    }

    @Transactional
    public void delete(Long id) {
        Agency agency = userService.getCurrentAgency();
        if (agency == null) {
            throw new AccessDeniedException();
        }

        Optional<Product> product = productRepository.findById(id);
        if (product.isPresent() && agency.getId().equals(product.get().getAgencyID())) {
            product.get().setActivated(false);
            productRepository.save(product.get());
        } else {
            throw new ResourceNotFoundException();
        }
    }
}
