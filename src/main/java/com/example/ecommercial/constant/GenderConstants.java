package com.example.ecommercial.constant;

public final class GenderConstants {

    public static final String MALE = "male";

    public static final String FEMALE = "female";

    public static final String OTHER = "other";

    private GenderConstants() {}
}
