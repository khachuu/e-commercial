package com.example.ecommercial.model.class_id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

@NoArgsConstructor
@Getter
@Setter
public class TransactionProductId implements Serializable {
    private Long transactionID;
    private Long productID;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TransactionProductId that)) return false;
        return Objects.equals(transactionID, that.transactionID) && Objects.equals(productID, that.productID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionID, productID);
    }
}
