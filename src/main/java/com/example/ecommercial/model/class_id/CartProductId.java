package com.example.ecommercial.model.class_id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

@NoArgsConstructor
@Getter
@Setter
public class CartProductId implements Serializable {
    private Long productID;
    private Long cartID;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CartProductId that)) return false;
        return Objects.equals(productID, that.productID) && Objects.equals(cartID, that.cartID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productID, cartID);
    }
}
