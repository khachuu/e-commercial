package com.example.ecommercial.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

@Entity(name = "product")
@Getter
@Setter
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "agency_id")
    private Long agencyID;

    private String name;

    private String description;

    private BigDecimal price;

    private Long stockQuantity;

    private Instant createdAt;

    private Instant updatedAt;

    private boolean activated;
}
