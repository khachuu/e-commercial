package com.example.ecommercial.model;

import jakarta.persistence.*;


import java.io.Serializable;
import java.time.Instant;
import java.util.Collection;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity(name = "user")
@Getter
@Setter
public class User extends BaseUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private boolean activated;

}
