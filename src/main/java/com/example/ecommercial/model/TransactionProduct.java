package com.example.ecommercial.model;

import com.example.ecommercial.model.class_id.TransactionProductId;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Entity(name = "transaction_product")
@Getter
@Setter
@IdClass(TransactionProductId.class)
public class TransactionProduct implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "transaction_id")
    private Long transactionID;

    @Id
    @Column(name = "product_id")
    private Long productID;

    private BigDecimal price;

    private Long productQuantity;

    private BigDecimal discount;

    private BigDecimal tax;

    private BigDecimal shippingFee;

    private BigDecimal otherFee;

}
