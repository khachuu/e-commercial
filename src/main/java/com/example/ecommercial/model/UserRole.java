package com.example.ecommercial.model;

import com.example.ecommercial.model.class_id.TransactionProductId;
import com.example.ecommercial.model.class_id.UserRoleId;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.Instant;

@Entity(name = "user_role")
@Getter
@Setter
@IdClass(UserRoleId.class)
public class UserRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long userID;

    @Id
    private String roleName;

    private Instant createdAt;

}
