package com.example.ecommercial.model;

import com.example.ecommercial.model.class_id.CartProductId;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Entity(name = "cart_product")
@Getter
@Setter
@IdClass(CartProductId.class)
public class CartProduct implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "product_id")
    private Long productID;

    @Id
    @Column(name = "cart_id")
    private Long cartID;

    @Column(name = "product_quantity")
    private Long productQuantity;

}
